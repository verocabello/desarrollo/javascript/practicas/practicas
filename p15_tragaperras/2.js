//creamos array de frutas con la ruta y el nombre de la imagen correspondiente
	let frutas = ["imagenes/limon.png","imagenes/cereza.png","imagenes/sandia.png",
				  "imagenes/uvas.png","imagenes/platano.png","imagenes/fresa.png","imagenes/naranja.png"];
	let jugada =[];	
		  
				  
	//Escuchamos cualquier evento que se produzca al cargar la página
    window.addEventListener("load",(e)=>{
    	creando_imagenes_carrusel();	
	



    		let npartidas = 0;//Inicializamos el contador de partidas a 0

    		//Escuchamos el evento que se produce al hacer click en el boton de introduccion de monedas.
    	document.querySelector("#moneda>button").addEventListener("click",(e)=>{
    		npartidas++; //con cada click se añade una partida mas al contador
    		document.querySelector("#partidas").innerHTML = npartidas;//Se muestra el contador de partidas en el elemento correspondiente

    	});
    	//Escuchamos el evento que seproduce al hacer click en el boton de comenzar a jugar una partida.
    	document.querySelector("#jugar>button").addEventListener("click",(e)=>{
    		eliminar_caras();
    		if(npartidas >0){//Siempre que existan partidas en el contador se podrá realizar una nueva tirada
    			
    			//Al tener solo 3 imagenes posibles en el juego vamos generando aleatoriamente el cambio de imagen en cada una de ellas.
    			for(let c=0;c<3;c++){
    				let numero = palanca();
					document.querySelector("#fruta"+(c+1)).src = frutas[numero];
					jugada[c] = numero;
    			}
    			




				npartidas--;//Cuando se ha jugado una nueva partida se decrementa el contador de partidas
				
    			document.querySelector("#partidas").innerHTML = npartidas;
    			comprobar_premio(jugada,npartidas);
    			
    		}

    	});

    	document.querySelector("#cobrar>button").addEventListener("click",(e)=>{

    		location.reload();


    	});

	 	

    });

    //funcion que genera imgs dentro de los figure para despues ser animadas
     function creando_imagenes_carrusel(){
     	//console.log(frutas);
     	for(c=0;c<frutas.length;c++){
     		let imagen_carrusel = new Image(260,220);
     		imagen_carrusel.src = frutas[c];
			document.querySelector("#caja1").appendChild(imagen_carrusel);
			document.querySelector("#caja2").appendChild(imagen_carrusel);
			document.querySelector("#caja3").appendChild(imagen_carrusel);
			//document.querySelector("#img_err").appendChild(div);
     	}	
		
	}
    	//funcion que genera la imagen aleatoria den entre las 6 posibles
		function palanca() {
	  		return parseInt(Math.random() * (0 - 6)) + 6;
		}
		function comprobar_premio(jugada,partidas_actuales){
			//console.log("Partidas actuales:"+partidas_actuales);
			let premio=[];
			let contador = 0;
			for(let j=0;j<jugada.length;j++){
				premio[j]={};
				elemento = jugada[j];
				for(let p=0;p<jugada.length;p++){
					if(jugada[p] == elemento){
						contador++;
					}
				}	
					premio[j]['elemento'] = elemento;
					premio[j]['veces'] = contador;
					contador=0;	

			}

			//console.log(premio[0],premio[0]['elemento']);
			//eliminamos las imagenes ya existentes de premios

			//eliminar_caras();

			if((premio[0]['elemento'] == 1 && premio[0]['veces']==1 && premio[1]['veces'] !=2 && premio[2]['veces'] !=2) || (premio[1]['elemento'] == 1  && premio[1]['veces']==1 && premio[0]['veces'] !=2 && premio[2]['veces'] !=2) 
						|| (premio[2]['elemento'] == 1  && premio[2]['veces']==1 && premio[0]['veces'] !=2 && premio[1]['veces'] !=2)){
				document.querySelector("#premio").innerHTML = 1;
				let imagen_cara = new Image(50,50);
				imagen_cara.src = 'imagenes/cara_premio.png';
				document.querySelector("#cara").appendChild(imagen_cara);
			

			}else if((premio[0]['elemento'] == 1 && premio[1]['elemento']==1 && premio[0]['veces']==2 ) 
									|| (premio[1]['elemento'] == 1 && premio[2]['elemento']==1 && premio[1]['veces']==2)
							        || (premio[0]['elemento'] == 1 && premio[2]['elemento']==1 && premio[2]['veces']==2)){
				console.log(premio);
				document.querySelector("#premio").innerHTML = 4;
				let imagen_cara = new Image(50,50);
				imagen_cara.src = 'imagenes/cara_premio.png';
				document.querySelector("#cara").appendChild(imagen_cara);
			

			}else if(((premio[0]['elemento'] == 1 && premio[0]['veces']==3) && (premio[1]['elemento'] == 1  && premio[0]['veces']==3) 
				              && (premio[2]['elemento'] == 1 && premio[0]['veces']==3))){

				document.querySelector("#premio").innerHTML = 10;
				let imagen_cara = new Image(50,50);
				imagen_cara.src = 'imagenes/cara_premio.png';
				document.querySelector("#cara").appendChild(imagen_cara);
			
			}else if( (premio[0]['elemento'] != 1 && premio[1]['elemento'] !=1 && premio[2]['elemento'] != 1) 
				              && (premio[0]['veces'] == 2 || premio[1]['veces']==2 || premio[2]['veces']==2)){

				document.querySelector("#premio").innerHTML = 2;
				let imagen_cara = new Image(50,50);
				imagen_cara.src = 'imagenes/cara_premio.png';
				document.querySelector("#cara").appendChild(imagen_cara);
			
			}else if( (premio[0]['elemento'] != 1 && premio[1]['elemento'] !=1 && premio[2]['elemento'] != 1) 
				              && (premio[0]['veces'] == 3 || premio[1]['veces'] == 3 || premio[2]['veces'] ==3)){

				document.querySelector("#premio").innerHTML = 5;
				let imagen_cara = new Image(50,50);
				imagen_cara.src = 'imagenes/cara_premio.png';
				document.querySelector("#cara").appendChild(imagen_cara);
			
			}else if(  (premio[0]['elemento'] == 1 && (premio[1]['elemento'] != 1 && premio[2]['elemento'] != 1 && premio[1]['elemento'] == premio[2]['elemento'])) 
					|| (premio[1]['elemento'] == 1 && (premio[0]['elemento'] != 1 && premio[2]['elemento'] != 1  && premio[0]['elemento'] == premio[2]['elemento'])) 
					|| (premio[2]['elemento'] == 1 && (premio[0]['elemento'] != 1 && premio[1]['elemento'] != 1  && premio[0]['elemento'] == premio[1]['elemento']))  	

				){

				document.querySelector("#premio").innerHTML = 3;
				let imagen_cara = new Image(50,50);
				imagen_cara.src = 'imagenes/cara_premio.png';
				document.querySelector("#cara").appendChild(imagen_cara);
			}else{
				let imagen_cara = new Image(50,50);
				imagen_cara.src = 'imagenes/cara_sin_premio.png';
				document.querySelector("#cara").appendChild(imagen_cara);
			}
		}

		function eliminar_caras(){
			 let eliminar_caras = Array.prototype.slice.call(document.querySelectorAll("#cara>img"), 0);

			  for(element of eliminar_caras){
			    
			    element.remove();
			    
			  }  
		}
		
